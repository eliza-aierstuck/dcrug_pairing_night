# grabbing the weather
## your task

Using the OpenWeatherMap API at http://openweathermap.org/ current, create a
program that prompts for a city name and returns the current temperature for the
city.

## example output

```
Where are you? Chicago IL
Chicago weather:
65 degrees Fahrenheit
```

## constraint

Keep the processing of the weather feed separate from the part of your program
that displays the results.

## challenges

* The API gives the sunrise and sunset times, as well as the humidity and a
  description of the weather. Display that data in a meaningful way
* The API gives the wind direction in degrees. Convert it to words such as “North,” “West,”
  “South,” “South-west,” or even “South-southwest.
* Develop a scheme that lets the weather program tell you what kind of day it is. If it’s
  70 degrees and clear skies, say that it’s a nice day out
* Display the temperature in both Celsius and Fahrenheit
* Based on the information, determine if the person needs a coat or an umbrella.
