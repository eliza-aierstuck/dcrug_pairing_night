# text sharing
## your task

Create a web application that lets users share a snippet of text, similar to
https://pastebin.com/. The program you write should follow these specifications:

* The user should enter the text into a text area and save the text.
* The text should be stored in a data store.
* The program should generate a URL that can be used to retrieve the saved text.
* When a user follows that URL, the text should be dis-played, along with an
  invitation to edit the text.
* When a user clicks the Edit button, the text should be copied and placed in the
  same interface used to create new text snippets.

## challenges

* Modify the program so that each paste supports Markdown formatting.
* Modify the program so that the edit functionality edits the existing node and keeps
  versions of previous notes.
