# magic 8 ball
## your task

Arrays are great for storing possible responses from a pro- gram. If you combine
that with a random number generator, you can pick a random entry from this list,
which works great for games. Create a Magic 8 Ball game that prompts for a
question and then displays either “Yes,” “No,” “Maybe,” or “Ask again later.”

## example output

```
What's your question?
Will I be rich and famous?
Ask again later.
```

## constraint

The value should be chosen randomly using a pseudo random number generator.
Store the possible choices in a list and select one at random.
