# password generator
## your task

Create a program that generates a secure password. Prompt the user for the
minimum length, the number of special characters, and the number of numbers.
Then generate a password for the user using those inputs.

## example output

```
What's the minimum length?
8
How many special characters?
2
How many numbers?
2
Your password is
aurn2$1s#
```

## constraints

* Use lists to store the characters you’ll use to generate the passwords.
* Add some randomness to the password generation.

## challenges

* Randomly convert vowels to numbers, such as 3 for E and 4 for A.
* Have the program present a few options rather than a single result.
* Place the password on the user’s clipboard when generated.
