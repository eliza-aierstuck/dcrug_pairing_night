# filtering records
## your task

Given the following data set create a program that lets a user locate all
records that match the search string by comparing the search string to the first
or last name field.

```
First Name   Last Name     Position             Separation date
John         Johnson       Manager              2016-12-31
Tou          Xiong         Software Engineer    2016-10-05
Michaela     Jacobson      District Manager     2015-12-19
Jake         Jackson       Programmer
Jacquelyn    Weber         DBA
Sally        Michaelson    Web Developer        2015-12-18
```

## example output
```
Enter a search string:  Jac
Results:
Name                | Position          | Separation Date
--------------------|-------------------|----------------
Jacquelyn Jackson   | DBA               |
Jake Jacobson       | Programmer        |
```

## constraint

Implement the data using an array of maps or an associative array.

## challenges

* Make the search case insensitive.
* Add the option to search by position.
* Add the option to find all employees where their separation date is six months ago or more.
* Read in the data from a file.
