# trivia app
## your task

Create a multiple-choice trivia application.

* Read questions, answers, and distractors (wrong answers) from a file.
* When a player starts a game

– Choose questions at random.

– Display the answer and distractors in a random order.

– Track the number of correct answers.

– End the game when the player selects a wrong answer.

## challenges

* Add a difficulty field for the questions, and present increasingly difficult
  questions as the game progresses.
* Expand the program by adding a mode that allows a user to add, edit, or remove
  questions and answers.
