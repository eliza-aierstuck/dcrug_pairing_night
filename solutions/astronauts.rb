require 'httparty'

def print_table(astronauts, name_column_width)
  astronauts.each do |person|
    puts "#{right_pad(person['name'], name_column_width)}| #{person['craft']}"
  end
end

def print_header(name_column_width)
  puts "#{right_pad('Name', name_column_width)}| Craft"
  puts '-' * name_column_width + '|-------'
end

def column_width(items)
  items.max_by(&:length).length
end

def right_pad(item, column_width)
  item.to_s + ' ' * (column_width - item.length)
end

response = HTTParty.get('http://api.open-notify.org/astros.json')
parsed_json = JSON.parse(response.body)
astronauts = parsed_json['people']
astronaut_names = astronauts.map { |person| person['name'] }
name_column_width = column_width(astronaut_names)

print_header(name_column_width)
print_table(astronauts, name_column_width)


