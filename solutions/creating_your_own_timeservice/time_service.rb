require 'grape'

module TimeService
  class API < Grape::API
    format :json
    prefix :api

    get :current_time do
      Time.now.utc
    end
  end
end