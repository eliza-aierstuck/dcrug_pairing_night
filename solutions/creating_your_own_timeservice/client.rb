require 'httparty'

class Client
  def initialize(base_url)
    @base_url = base_url
  end

  def display_time
    response = HTTParty.get("#{@base_url}/api/current_time")
    puts "The current time is #{response}"
  end
end