# dcrug pair programming night

In this repo you'll find a list of challenges of varying difficulty levels.

## Pairing Etiquette

**Pairing Dialogue:**

Try the following:
```
Saying “I think...” to propose an idea.
Asking “Do we agree...” to confirm agreement
Saying “We agree...” to discuss progress towards goals
Avoid saying “No” / ”But”. Instead try saying, “Yes and...”
Ask “What would it take to get you in..” if you’re disagree about a proposed idea
```

**Check your 4 “C”’s:**
* Communication: Do you agree you’re both communicating well with each other?
* Comfort: Are you two both comfortable with the context you’re working in?
* Confidence: Are you confident in your skills and your teams’, or at least voiced fears?
* Compromise: Are you willing to make compromises in order to make progress?

## **Use [codeshare.io](https://codeshare.io/) for pairing**
## **Maybe run your code with [repl.it?](https://repl.it/)
