# creating your own time service
## your task

Create a simple web service that returns the current time as JSON data, such as:
{ "currentTime": "2050-01-24 15:06:26" }. Then create a client application that
connects to the web service, parses the response, and displays the time.

## example output

The current time is 15:06:26 UTC January 4 2050.

## constraint

Build the server app with as little code as possible.
