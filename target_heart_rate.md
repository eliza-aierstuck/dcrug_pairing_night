# target heart rate
## your task

When getting into a fitness program, you may want to figure out your target
heart rate so you don’t overexert yourself. The Karvonen heart rate formula is
one method you can use to determine your rate. Create a program that prompts for
your age and your resting heart rate. Use the Karvonen for- mula to determine
the target heart rate based on a range of intensities from 55% to 95%. Generate
a table with the results as shown in the example output. The formula
is `TargetHeartRate = (((220 − age) − restingHR) × intensity) + restingHR`

## example output

```
Resting Pulse: 65    Age: 22

Intensity    | Rate
-------------|--------
55%          | 138 bpm
60%          | 145 bpm
65%          | 151 bpm
```

## constraints

* Don’t hard-code the percentages. Use a loop to incre- ment the percentages from
55 to 95.
* Ensure that the heart rate and age are entered as num- bers. Don’t allow the
user to continue without entering valid inputs.
* Display the results in a tabular format.
