# word frequency finder
## your tasks

Create a program that reads in a file and counts the frequency of words in the
file. Then construct a histogram displaying the words and the frequency, and
display the histogram to the screen.

## example output

Given the text file words.txt with this content:

`badger badger badger badger mushroom mushroom snake badger badger badger`

the program would produce the following output:

```
badger:   *******
mushroom: **
snake:    *
```

## constraint

* Ensure that the most used word is at the top of the report and the least used
  words are at the bottom.

## challenges

* Use a graphical program and generate bar graphs.
* Test the performance of your calculation by providing a very large input file,
  such as Shakespeare’s [Macbeth](http://shakespeare.mit.edu/macbeth/full.html).
  Tweak your algorithm so that it performs the word counting as fast as possible.
* Write the program in another language and compare the processing times of the
  two implementations.

